import { rescaleImportedPolygons } from './client/lib/rescaleImportedPolygons.js';

const RoiScaler = {
  rescaleImportedPolygons
};

export {
  RoiScaler
};
